using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(PixelRoomBuilder))]
public class PixelRoomBuilderEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        PixelRoomBuilder prbTarget = (PixelRoomBuilder)target;

        // Change Floor Vertical Size.
        EditorGUILayout.BeginHorizontal();
        prbTarget.FloorLength = EditorGUILayout.IntField("Floor Length", prbTarget.FloorLength);
        if (prbTarget.FloorLength <= 0)
        {
            prbTarget.FloorLength = 0;
        }
        if (GUILayout.Button("+"))
        {
            prbTarget.FloorLength++;
            prbTarget.ChangeRoomSize();
            EditorUtility.SetDirty(prbTarget);
        }

        if (GUILayout.Button("-"))
        {
            if (prbTarget.FloorLength <= 0)
            {
                prbTarget.FloorLength = 0;
            }
            else
            {
                prbTarget.FloorLength--;
                prbTarget.ChangeRoomSize();
                EditorUtility.SetDirty(prbTarget);
            }
        }
        EditorGUILayout.EndHorizontal();

        // Change Floor Horizontal Size.
        EditorGUILayout.BeginHorizontal();
        prbTarget.FloorWeight = EditorGUILayout.IntField("Floor Weight", prbTarget.FloorWeight);
        if (prbTarget.FloorWeight <= 0)
        {
            prbTarget.FloorWeight = 0;
        }
        if (GUILayout.Button("+"))
        {
            prbTarget.FloorWeight++;
            prbTarget.ChangeRoomSize();
            EditorUtility.SetDirty(prbTarget);
        }
        if (GUILayout.Button("-"))
        {
            if (prbTarget.FloorWeight <= 0)
            {
                prbTarget.FloorWeight = 0;
            }
            else
            {
                prbTarget.FloorWeight--;
                prbTarget.ChangeRoomSize();
                EditorUtility.SetDirty(prbTarget);
            }   
        }
        EditorGUILayout.EndHorizontal();

        // Change Wall Height.
        EditorGUILayout.BeginHorizontal();
        prbTarget.WallHeight = EditorGUILayout.IntField("Wall Height", prbTarget.WallHeight);
        if (prbTarget.WallHeight <= 0)
        {
            prbTarget.WallHeight = 0;
        }
        if (GUILayout.Button("+"))
        {
            prbTarget.WallHeight++;
            prbTarget.ChangeRoomSize();
            EditorUtility.SetDirty(prbTarget);
        }
        if (GUILayout.Button("-"))
        {
            if (prbTarget.WallHeight <= 0)
            {
                prbTarget.WallHeight = 0;
            }
            else
            {
                prbTarget.WallHeight--;
                prbTarget.ChangeRoomSize();
                EditorUtility.SetDirty(prbTarget);
            }               
        }
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        // Build a room.
        if (GUILayout.Button("Build"))
        {
            prbTarget.BuildARoom();
            EditorUtility.SetDirty(prbTarget);
        }

        if (prbTarget.FloorLength != prbTarget.CurrFloorLength || prbTarget.FloorWeight != prbTarget.CurrFloorWeight || prbTarget.WallHeight != prbTarget.CurrWallHeight)
        {
            prbTarget.ChangeRoomSize();
        }

        // Clear all Object.
        if (GUILayout.Button("Clean"))
        {
            prbTarget.ClearAllObject();
            EditorUtility.SetDirty(prbTarget);
        }
        EditorGUILayout.EndHorizontal();
    }
}