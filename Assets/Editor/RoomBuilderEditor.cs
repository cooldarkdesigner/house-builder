using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(RoomBuilder))]
public class RoomBuilderEditor : Editor
{

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        RoomBuilder rbTarget = (RoomBuilder)target;

        EditorGUILayout.BeginHorizontal();
        // Build a room.
        if (GUILayout.Button("Build"))
        {
            rbTarget.BuildARoom();
            EditorUtility.SetDirty(rbTarget);
        }

        // Clear all Object.
        if (GUILayout.Button("Clean"))
        {
            rbTarget.ClearAllObject();
            EditorUtility.SetDirty(rbTarget);
        }
        EditorGUILayout.EndHorizontal();

        if (rbTarget.FloorLength != rbTarget.CurrFloorLength || rbTarget.FloorWeight != rbTarget.CurrFloorWeight || rbTarget.WallHeight != rbTarget.CurrWallHeight)
        {
            rbTarget.ChangeRoomSize();
        }
    }
}
