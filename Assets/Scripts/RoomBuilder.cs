using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("House Builder/Room Builder")]
public class RoomBuilder : MonoBehaviour
{
    [SerializeField]    
    private Transform wallPrefab;
    [SerializeField]
    private Transform floorPrefab;

    private Transform[] walls = new Transform[4];
    private Transform floor;

    [SerializeField]
    private float floorLength = 5f;
    public float FloorLength 
    { 
        get => floorLength; 
        set
        {
            floorLength = wallHeight < 0 ? 0 : value;
        }
    }
    [SerializeField]
    private float floorWeight = 5f;
    public float FloorWeight
    {
        get => floorWeight;
        set
        {
            floorWeight = wallHeight < 0 ? 0 : value;
        }
    }
    [SerializeField]
    private float wallHeight = 5f;
    public float WallHeight
    {
        get => wallHeight;
        set
        {
            wallHeight = wallHeight < 0 ? 0 : value; 
        }
    }

    private float currFloorLength;
    public float CurrFloorLength { get => currFloorLength; set => currFloorLength = value; }
    private float currFloorWeight;
    public float CurrFloorWeight { get => currFloorWeight; set => currFloorWeight = value; }
    private float currWallHeight;
    public float CurrWallHeight { get => currWallHeight; set => currWallHeight = value; }

    private bool isBuilded = false;

    public void BuildARoom()
    {
        if (!isBuilded)
        {
            if (floor == null)
            {
                floor = Instantiate(floorPrefab);
                floor.localPosition = new Vector3(FloorWeight / 2, 0f, FloorLength / 2);
                floor.localEulerAngles = new Vector3(90f, 0f, 0f);
                floor.localScale = new Vector3(floorWeight, floorLength, 1f);
                floor.SetParent(this.transform);
            }

            if (walls[0] == null)
            {
                for (int i = 0; i < walls.Length; i++)
                {
                    Transform tempWall = Instantiate(wallPrefab);
                    // forward[0] -> back[1] -> left[2] -> right[3]
                    switch (i)
                    {
                        case 0:
                            tempWall.localPosition = new Vector3((float)floorWeight / 2f, (float)wallHeight / 2f, floorLength);
                            tempWall.localEulerAngles = new Vector3(0f, 0f, 0f);
                            tempWall.localScale = new Vector3(floorWeight, wallHeight, 1f);
                            break;
                        case 1:
                            tempWall.localPosition = new Vector3((float)floorWeight / 2f, (float)wallHeight / 2f, 0f);
                            tempWall.localEulerAngles = new Vector3(0f, 180f, 0f);
                            tempWall.localScale = new Vector3(floorWeight, wallHeight, 1f);
                            break;
                        case 2:
                            tempWall.localPosition = new Vector3(floorWeight, (float)wallHeight / 2f, (float)floorLength / 2f);
                            tempWall.localEulerAngles = new Vector3(0f, 90f, 0f);
                            tempWall.localScale = new Vector3(floorLength, wallHeight, 1f);
                            break;
                        case 3:
                            tempWall.localPosition = new Vector3(0f, (float)wallHeight / 2f, (float)floorLength / 2f);
                            tempWall.localEulerAngles = new Vector3(0f, -90f, 0f);
                            tempWall.localScale = new Vector3(floorLength, wallHeight, 1f);
                            break;
                    }
                    tempWall.name = "Wall_" + i;
                    tempWall.SetParent(this.gameObject.transform);
                    walls[i] = tempWall;
                }
            }

            currFloorLength = floorLength;
            currFloorWeight = floorWeight;
            currWallHeight = wallHeight;

            isBuilded = true;
        }
    }

    public void ChangeRoomSize()
    {
        if(floor == null)
        {
            return;
        }
        else
        {
            floor.localPosition = new Vector3(FloorWeight / 2f, 0f, FloorLength / 2f);
            floor.localScale = new Vector3(floorWeight, floorLength, 1f);

            for (int i = 0; i < walls.Length; i++)
            {
                switch (i)
                {
                    case 0:
                        walls[i].localPosition = new Vector3(floorWeight / 2f, wallHeight / 2f, floorLength);
                        walls[i].localScale = new Vector3(floorWeight, wallHeight, 1f);
                        break;
                    case 1:
                        walls[i].localPosition = new Vector3(floorWeight / 2f, wallHeight / 2f, 0f);
                        walls[i].localScale = new Vector3(floorWeight, wallHeight, 1f);
                        break;
                    case 2:
                        walls[i].localPosition = new Vector3(floorWeight, wallHeight / 2f, floorLength / 2f);
                        walls[i].localScale = new Vector3(floorLength, wallHeight, 1f);
                        break;
                    case 3:
                        walls[i].localPosition = new Vector3(0f, wallHeight / 2f, floorLength / 2f);
                        walls[i].localScale = new Vector3(floorLength, wallHeight, 1f);
                        break;
                }
            }

            currFloorLength = floorLength;
            currFloorWeight = floorWeight;
            currWallHeight = wallHeight;
        }      
    }

    public void ClearAllObject()
    {
        // Clear walls.
        for (int i = 0; i < walls.Length; i++)
        {
            DestroyImmediate(walls[i].gameObject, true);
            walls[i] = null;
        }

        // Clear floor.
        DestroyImmediate(floor.gameObject, true);

        isBuilded = false;
    }
}